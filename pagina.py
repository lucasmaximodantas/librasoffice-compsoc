"""
O objetivo do programa é ler um arquivo .txt, ao qual o ultimo item adicionado ao arquivo,
será plotado em uma janela em formato de gif, correspondente ao nome do item.

(OBS: a posição do gif, corresponde com o tamanho da tela)

*** Melhorias futuras ***

1_ ajustar o tamanho da tela

2_deslocar os gifs para um ficheiro separado

"""
import sys
from PySide2.QtGui import QMovie
import time
import os
from PySide2.QtWidgets import *
from PySide2.QtQuick import *
from PySide2.QtCore import *
from multiprocessing import Process



class Controle(object):
    """Clase para controla da janela"""

    def __init__(self):
        self.diretorio = os.path.dirname(os.path.realpath(__file__))
        print(self.diretorio)
        self.nome = None
        self.player = None
        self.nome_atual = "idavdidb.gif"


        while True:
            self.ler()
            if self.nome == "":
                continue
            subProcesso = Process(target=self.mostrar_gif)
            subProcesso.start()
            while True:
                time.sleep(0.2)
                self.ler()
                if self.nome == self.nome_atual:
                    continue
                elif self.nome == "":
                    subProcesso.terminate()
                    break
                else:
                    subProcesso.terminate()
                    subProcesso = Process(target=self.mostrar_gif)
                    subProcesso.start()
                    self.nome_atual = self.nome

    def mostrar_gif(self):
        giffile = self.nome+".gif"
        app = QApplication([])
        view = QQuickView()
        contexto = view.rootContext()
        #Por algum motivo o arquivo não abre se não estiver na mesma pasta
        contexto.setContextProperty('librasGif', giffile)
        view.setFlag(Qt.FramelessWindowHint)
        url = QUrl("pagina.qml")
        view.setSource(url)
        view.show()
        tamanhoDaTela = app.desktop().screenGeometry()

        X = tamanhoDaTela.width() - tamanhoDaTela.width()/10  #210  #1920  #1280
        Y = tamanhoDaTela.height() - tamanhoDaTela.height()/4 #260 #1080  #720

        view.setPosition(X, Y)

        app.exec_()



    def ler(self):
        f = open("Leitura.txt", 'r')
        i = 0
        for i in f:
            print(" Linhas:  "+i)
        f.close()

        if i == 0:
            self.nome = ""
        else:
            self.nome = i.rstrip('\n')


if __name__ == "__main__":
    c = Controle()
